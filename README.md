# FairCoin.Co electrumfairchainsx server

electrumfairchainsx server for electrumfair(chains) wallets

The service is made for/controlled by gitlab runner.

## Usage

### by Gitlab-Runner

1. Fork this project to your group/repositories where you have set up a gitlab-runner
1. change the gitlab-runner tags in .gitlab-ci.yml
1. add env files ( select Type = file ! ) to override defaults ./env_file , go to Gitlab **Settings** -> **CI/CD** -> **Variables**

You can read a full developer report of server preparation with gitlab-runner usage > [DEV_REPORT.md](DEV_REPORT.md)

~~~
#### env files ( examples see env_file/... / select Type = file !) ######################
FAIRCHAINS_CONF                 # [optional] fairchains.conf / faircoin.conf
SSL_CONF                        # [mandatory] configuration to create ssl cert
FAIRCHAINS_ELECTRUMX_JSON       # [optional] electrumx serve network configuration
FAIRCHAINS_JSON                 # [optional] blockchain parameters
~~~
1. run command, go to Gitlab **CI/CD** -> **Pipeline** and **Run Pipeline**
Enter variable name **CMD**
~~~
build        # build container ( changes of Dockerfile )
start        # start container ( changes of scripts )
new_ssl      # create new ssl file for electrumx server
stop         # stop container
install      # runs build, start and new_ssl stages
uninstall    # remove container without to delete the data
remove       # remove all data
~~~


### by SSH access <small>( manually without gitlab-runner )</small>

1. install docker and docker-compose ( https://docs.docker.com/compose/install/ )
1. clone this project
1. change configuration in ./env_file
1. Initialize env vars
~~~
chmod +x setenv
. setenv
~~~

1. run command from project root folder
~~~
deploy/build
deploy/start
deploy/new_ssl
deploy/stop
deploy/install
deploy/uninstall
deploy/remove
~~~

#### Troubleshooting

some commands can be helpful getting shell access to container or get some status info
~~~
console/electrumx                   # get access to container of electrumfairchainsx server ( same as electrumfairchainsx )
console/electrumfairchainsx         # get access to container of electrumfairchainsx server
console/faircoin                    # get access to container of faircoind

console/ps                          # list containers status info
console/status                      # list all ports / processes and faircoind info
~~~
